﻿using System;
using System.Collections.Generic;

using SFML.Graphics;
using SFML.System;
using System.Diagnostics;

namespace PS
{
    public static class Time
    {
        //Settings
        public static Single timeScale = 1f;
        public static readonly float FixedRenderRate = 0.01f;
        public static readonly float FixedDebugRate = 0.25f;

        //Totals
        public static Single realTime = 0f;
        public static Single scaledTime = 0f;
        public static Single fixedTime = 0f;

        //Deltas
        public static Single deltaRealTime = 0f;
        public static Single deltaScaledTime = 0f;
        public static Single deltaFixedTime = 0f;

        //Other
        public static uint calcFrame = 0;
        public static uint fixedFrame = 0;
        public static Single calcFPS = 100f;
        public static Single fixedFPS = 60f;

        //Internals
        private static Single lastRealTime = 0f;
        private static Single lastScaledTime = 0f;
        private static Single lastFixedTime = 0f;
        private static Single lastDebugTime = 0f;

        private static Stopwatch gameStopWatch = new Stopwatch();

        static Time()
        {
            gameStopWatch.Start();
        }

        //Functions
        public static void StepTime()
        {
            lastRealTime = realTime;
            realTime = (Single)gameStopWatch.Elapsed.TotalSeconds;
            deltaRealTime = realTime - lastRealTime;

            deltaScaledTime = timeScale * deltaRealTime;
            lastRealTime = scaledTime;
            scaledTime += deltaScaledTime;

            calcFrame++;
            calcFPS = (calcFPS * 49f + 1f / deltaRealTime) / 50f;
        }

        public static bool TryStepFixed()
        {
            if(gameStopWatch.Elapsed.TotalSeconds - lastFixedTime > FixedRenderRate)
            {
                lastFixedTime = fixedTime;
                fixedTime = (Single)gameStopWatch.Elapsed.TotalSeconds;
                deltaFixedTime = fixedTime - lastFixedTime;

                fixedFrame++;
                fixedFPS = (fixedFPS * 49f + 1f / deltaFixedTime) / 50f;

                return true;
            } 
            return false;
        }

        public static bool TryStepDebug()
        {
            if (gameStopWatch.Elapsed.TotalSeconds - lastDebugTime > FixedDebugRate)
            {
                lastDebugTime = (Single)gameStopWatch.Elapsed.TotalSeconds;
                return true;
            }
            return false;
        }

    }
    
}
