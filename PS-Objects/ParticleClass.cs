﻿using System;
using System.Collections.Generic;

using SFML.Graphics;
using SFML.System;

namespace PS
{
    public class Particle : CircleShape
    {
        private static int idCount = 0;

        public readonly int id = idCount++;

        public Single mass;
        public Vector2f heading;
        public Single speed;
        public Vector2f velocity
        {
            get
            {
                return heading * speed;
            }
        }

        public float Diameter
        {
            get
            {
                return Radius * 2.0f;
            }
        }

        public static Particle generate()
        {
            Particle p = new Particle();
            p.Radius = Settings.randInRange(5, 50);
            p.Position = new Vector2f(Settings.randInRange(0, Settings.GameSize.X), Settings.randInRange(0, Settings.GameSize.Y));
            Single bearing = (Single)Math.PI * Settings.randInRange(-1, 1);
            p.heading = new Vector2f((Single)Math.Cos(bearing), (Single)Math.Sin(bearing));
            p.speed = Settings.randInRange(Settings.seedSpeedRange) * 50.0f;
            Console.Out.WriteLine("Created Particle ID: " + p.id);
            return p;
        }
    }
    
}
