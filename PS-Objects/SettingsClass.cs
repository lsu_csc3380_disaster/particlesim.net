﻿using System;
using System.Collections.Generic;

using SFML.Graphics;
using SFML.System;

namespace PS
{
    public static class Settings
    {
        public static readonly Vector2u WindowSize = new Vector2u(1024, 512);
        public static readonly Vector2u GameSize = new Vector2u(4096, 4096);
        public static readonly Vector2u DebugSize = new Vector2u(512, 512);
        public static readonly Vector2f seedSpeedRange = new Vector2f(10f, 5f);
        public static readonly Vector2f seedDensityRange = new Vector2f(500f, 1f);
        public static Random rand = new Random();


        public static readonly int PT_TargetMaxLeafs = 2;
        public static readonly float PT_TargetActionFrames = 10;


        public static Single randInRange(Single min, Single max)
        {
            return (Single)rand.NextDouble() * (max - min) + min;
        }
        public static Single randInRange(Vector2f range)
        {
            return randInRange(range.X, range.Y);
        }
    }

}


