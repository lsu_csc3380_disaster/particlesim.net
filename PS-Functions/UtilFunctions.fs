﻿namespace PS

open System.Collections
open System.Collections.Generic;
open PS
open SFML.System
open SFML.Graphics

module public UtilFunctions =
    let Vector2fMagnitude ( v : Vector2f ) : single =
        sqrt ( v.X * v.X + v.Y * v.Y )

    let RectWithinRectBounds ( posA : Vector2f, sizeA : Vector2f, posB : Vector2f, sizeB : Vector2f) : bool =
        posB.X > posA.X &&
        posB.Y > posA.Y &&
        posB.X + sizeB.X < posA.X + sizeA.X &&
        posB.Y + sizeB.Y < posA.Y + sizeA.Y 
        
    let PointWithinRectBounds ( posA : Vector2f, sizeA : Vector2f, posB : Vector2f) =
        posB.X > posA.X &&
        posB.Y > posA.Y &&
        posB.X < posA.X + sizeA.X &&
        posB.Y < posA.Y + sizeA.Y 

    let checkRectCollision ( posA : Vector2f, sizeA : Vector2f, posB : Vector2f, sizeB : Vector2f) : bool =
        let superSize = Vector2f.op_Addition(sizeA, sizeB);
        let superPos = posA - sizeB / 2.0f;
        PointWithinRectBounds(superPos, superSize, posB);

    let generateRandomColor () : Color =
        let seed = Settings.rand.Next(1536)
        let value = (byte)(seed / 6);
        match seed % 6 with
        | 0 -> new Color(value, 255uy, 0uy)
        | 1 -> new Color(value, 0uy, 255uy)
        | 2 -> new Color(255uy, value, 0uy)
        | 3 -> new Color(0uy, value, 255uy)
        | 4 -> new Color(255uy, 0uy, value)
        | 5 -> new Color(0uy, 255uy, value)
        