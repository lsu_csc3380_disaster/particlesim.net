﻿namespace PS
namespace PS.ParticleCollection

open System.Collections
open System.Collections.Generic;
open PS
open SFML.System
open SFML.Graphics

open UtilFunctions
open ParticleFunctions
open ParticleTree_Node

module ParticleTree_Leaf= 
    exception UnexpectedCall of string
    type internal Leaf(_particle : Particle, _parent : Node) =
        class
            let mutable particle = _particle;
            let mutable parent = _parent;
            interface Node with
                member this.updatePosition (delta : single) = 
                    particle.Position <-
                        let i = this.position + this.velocity * delta
                        let bounds = new Vector2f(
                                                    (single)Settings.GameSize.X - this.diameter,
                                                    (single)Settings.GameSize.Y - this.diameter
                                                 )
                        let x =
                            if i.X < 0.0f then 
                                particle.heading.X <- abs(particle.heading.X);
                                0.0f
                            else if i.X > bounds.X then
                                particle.heading.X <- -abs(particle.heading.X);
                                bounds.X
                            else
                                i.X
                        let y = 
                            if i.Y < 0.0f then 
                                particle.heading.Y <- abs(particle.heading.Y);
                                0.0f
                            else if i.Y > bounds.Y then
                                particle.heading.Y <- -abs(particle.heading.Y);
                                bounds.Y
                            else
                                i.Y
                        //printfn "ID: %i Pos: %s" P.id ((Vector2f(x,y)).ToString())
                        Vector2f(x,y);
                member this.updateCollision (delta : single) =
                    ();
                member this.updateCompress (delta : single) =
                    ();
                member this.updateOrder (delta : single) =
                    ();
                member this.draw (_target : RenderTarget) = 
                    (_target.Draw(particle));
                member this.addNode(node : Node) = 
                    raise(UnexpectedCall("Leafs should not have nodes added."))
                member this.print( indention : int ) = 
                    printfn "%s- L%i" (new string('|', indention)) particle.id
            member this.position
                with get() = particle.Position;
            member this.velocity
                with get() = particle.velocity;
            member this.diameter
                with get() = particle.Diameter
            member this.size
                with get() = Vector2f(particle.Radius, particle.Radius);
            member this.color
                with get() = particle.FillColor;
                and set(c : Color) = particle.FillColor <- c;
        end