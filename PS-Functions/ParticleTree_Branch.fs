﻿namespace PS
namespace PS.ParticleCollection

open System.Collections
open System.Collections.Generic;
open PS
open SFML.System
open SFML.Graphics

open UtilFunctions
open ParticleFunctions
open ParticleTree_Node
open ParticleTree_Leaf

module ParticleTree_Branch = 

    type internal Branch(_pos_ : Vector2f, _size_ : Vector2f, _parent_ : Node) =
        class
            let mutable nodes : Node list = [ ]
            let mutable _pos = _pos_;
            let mutable _size = _size_;
            let mutable _parent = _parent_;
            let mutable _color = Color.White;
            static let mutable _BranchCount : int = 0;

            let _id = _BranchCount
            do 
                _BranchCount <- _id + 1;
                _color <- generateRandomColor();

            member this.color
                with get() = _color;
                and set(c : Color) = _color <- c;

            member this.position
                with get() = _pos;

            member this.size
                with get() = _size;

            interface Node with
                member this.updatePosition (delta : single) = 
                    for item in nodes do
                        item.updatePosition(delta)     
                member this.addNode(node : Node) = this.Add(node);
                member this.draw (_target : RenderTarget) = 
                    let myRect = new RectangleShape(this.size)
                    myRect.Position <- this.position
                    myRect.FillColor <- Color.Transparent
                    myRect.OutlineThickness <- 8.0f
                    myRect.OutlineColor <- this.color
                    _target.Draw(myRect)
                    for item in nodes do
                        item.draw(_target);
                        
                member this.print( indention : int ) = 
                    printfn "%s*- B%i" (new string('|', indention)) _id
                    for item in nodes do
                        item.print(indention + 1)
                member this.updateCollision ( delta : single ) = ();
                member this.updateOrder ( delta : single ) = 
                //Re-order the leafs and branches.
                //Send particle to parent unless it's the root node.
                    ();
                        
                member this.updateCompress( delta : single ) = 
                //Remove branches with less than the optimal number of elements,
                //Unless it is the root node.
                ();

            member this.Intersects ( p : Node ) : bool = false;
            member this.Engulfs ( p : Node ) : bool = 
                match p with
                | :? Branch as B -> 
                    RectWithinRectBounds( _pos, _size, B.position, B.size)
                | :? Leaf as L -> 
                    RectWithinRectBounds( _pos, _size, L.position, L.size)
                | _ -> false
                    
            member this.Add( p : Node ) = 
                if this.Engulfs( p ) then 
                    nodes <- 
                        if(nodes.Length = 0) then
                            match p with
                            | :? Leaf as L -> 
                                L.color <- this.color
                                [ L ]
                            | _ -> [ p ]
                        else 
                            let rec rAddToTree ( nodeToAdd : Node, nodeList : Node list) : Node list =
                                    if nodeList.IsEmpty then
                                        match nodeToAdd with
                                        | :? Leaf as L -> 
                                            L.color <- this.color
                                            [ L ]
                                        | _ -> [ nodeToAdd ]
                                    else
                                        match nodeList.Head with
                                        | :? Branch as B ->
                                            if B.Engulfs(nodeToAdd) then
                                                B.Add(nodeToAdd)
                                                nodeList
                                            else
                                                List.append [ nodeList.Head ] 
                                                    ( rAddToTree( nodeToAdd,  nodeList.Tail ) )
                                        | _ -> match nodeToAdd with
                                                | :? Branch as B ->
                                                    List.append [ B ] nodeList
                                                | :? Leaf as L ->
                                                    L.color <- this.color
                                                    List.append nodeList [ L ]
                            match nodes.Head with
                            | :? Leaf as L -> 
                                if nodes.Length < Settings.PT_TargetMaxLeafs then
                                    L.color <- this.color
                                    List.append [ p ] nodes
                                else
                                    if this.size.X / 2.0f > 1.0f then
                                        let offset = this.size / 2.0f;

                                        let mutable newNodes : Node list = 
                                            [
                                                (new Branch(this.position, offset, this) :> Node) 
                                                (new Branch(this.position + new Vector2f(offset.X,0.0f) ,offset, this) :> Node)
                                                (new Branch(this.position + new Vector2f(0.0f, offset.Y) ,offset, this) :> Node)
                                                (new Branch(this.position + this.size / 2.0f,offset, this) :> Node)
                                            ]

                                        List.fold (fun acc elem -> rAddToTree ( elem, acc ) ) newNodes nodes
                                    else
                                        L.color <- this.color
                                        List.append [ p ] nodes
                            | :? Branch as B -> 
                                rAddToTree( p,  nodes )
                    else
                        _parent.addNode(p)

        end