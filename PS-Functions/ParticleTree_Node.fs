﻿namespace PS
namespace PS.ParticleCollection

open System.Collections
open System.Collections.Generic;
open PS
open SFML.System
open SFML.Graphics

open UtilFunctions
open ParticleFunctions

module ParticleTree_Node = 
    type public Node =
        interface
            abstract updatePosition : single -> unit
            abstract updateCollision : single -> unit
            abstract updateOrder : single -> unit
            abstract updateCompress : single -> unit
            abstract draw : RenderTarget -> unit
            abstract print : int -> unit
            abstract addNode : Node -> unit
        end