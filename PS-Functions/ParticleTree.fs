﻿namespace PS
namespace PS.ParticleCollection

open System.Collections
open System.Collections.Generic;
open PS
open SFML.System
open SFML.Graphics

open UtilFunctions
open ParticleFunctions
open ParticleTree_Node
open ParticleTree_Leaf
open ParticleTree_Branch


type public ParticleTree() as this = 
    class
        let mutable Root : Branch = new Branch(new Vector2f(0.0f,0.0f), new Vector2f((single)Settings.GameSize.X,(single)Settings.GameSize.Y), this)
        do
            let bA = new Branch(new Vector2f(0.0f,0.0f), Root.size * 0.5f, Root)
            let bB = new Branch(new Vector2f(2048.0f,0.0f), Root.size * 0.5f, Root)
            let bC = new Branch(new Vector2f(0.0f,2048.0f), Root.size * 0.5f, Root)
            let bD = new Branch(new Vector2f(2048.0f,2048.0f), Root.size * 0.5f, Root)

            Root.Add(bA);
            Root.Add(bB);
            Root.Add(bC);
            Root.Add(bD);

        interface Node with
            member this.addNode(node : Node) = ();
            member this.updatePosition (delta : single) =
                ();
            member this.updateCollision (delta : single) =
                ();
            member this.updateCompress (delta : single) =
                ();
            member this.updateOrder (delta : single) =
                ();
            member this.draw (_target : RenderTarget) = (Root :> Node).draw(_target);
            member this.print( indention : int ) = 
                printfn "Particle Tree\n"
                (Root :> Node).print(1);
        member public this.Add ( p : Particle ) = Root.Add ( new Leaf ( p, Root ) );
        member public this.Update ( delta : single ) = 
            (Root :> Node).updatePosition(delta)
            (Root :> Node).updateOrder(delta)
            (Root :> Node).updateCompress(delta);
        member public this.UpdateFixed (delta : single ) = 
            (Root :> Node).updateOrder(delta)
            (Root :> Node).updateCompress(delta);
        member public this.Draw ( _target : RenderTarget ) = (Root :> Node).draw(_target);
        member public this.Print () = (this :> Node).print(0);
    end
