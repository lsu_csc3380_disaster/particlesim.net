﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SFML.Audio;
using SFML.Graphics;
using SFML.System;
using SFML.Window;

namespace PS
{
    public partial class SFMLProgram
    {
        float lastParticleAdditionTime = 0f;
        private void GameLoop()
        {
            pt.Update(Time.deltaScaledTime);
            if (Time.scaledTime - lastParticleAdditionTime > 1.0f)
            {
                pt.Add(Particle.generate());
                lastParticleAdditionTime = Time.scaledTime;
            }
        }

    }
}
