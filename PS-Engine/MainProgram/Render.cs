﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SFML.Audio;
using SFML.Graphics;
using SFML.System;
using SFML.Window;

namespace PS
{



    public partial class SFMLProgram
    {
        RenderWindow _window;
        RenderTexture PlayArea = new RenderTexture(Settings.GameSize.X, Settings.GameSize.Y);
        RenderTexture DebugArea = new RenderTexture(Settings.DebugSize.X, Settings.DebugSize.Y);

        Sprite PlayAreaSprite = new Sprite();
        Sprite DebugAreaSprite = new Sprite();
        Text FPSText;

        private void RenderInit()
        {
            _window = new RenderWindow(new VideoMode(Settings.WindowSize.X, Settings.WindowSize.Y), "SFML window");
            _window.SetVisible(true);
            _window.Closed += new EventHandler(OnClosed);

            font = new Font("DejaVuSansMono.ttf");

            PlayArea.Clear(new Color(255,255,255,200));

            FPSText = new Text("60.0", font);
            FPSText.CharacterSize = 32;
            FPSText.Color = Color.White;

            DebugAreaSprite.Position = new Vector2f(512, 0);
        }

        private void RenderPlay()
        {
            PlayArea.Clear(new Color(50,50,50));

            pt.Draw(PlayArea);


            PlayArea.Display();
            PlayAreaSprite.Texture = PlayArea.Texture;
            PlayAreaSprite.Scale = new Vector2f(0.125f, 0.125f);
        }

        private void RenderDebug()
        {
            DebugArea.Clear(Color.Black);

            string DebugTextString =
                "Ant Simulation Prototype - W. Craig Jones\n" +
                "Created 8/13/15\n-----\n" +
                "Run Time: " + Time.realTime + "\n" +
                "Scaled Time: " + Time.scaledTime + "\n-----\n" +
                "Calc FPS " + Time.calcFPS.ToString("F1") + "\n" +
                "Calc Frame " + Time.calcFrame.ToString("F1") + "\n-----\n" +
                "Fixed FPS " + Time.fixedFPS.ToString("F1") + "\n" +
                "Fixed Frame " + Time.fixedFrame.ToString("F1") + "\n";

            Text DebugText = new Text(DebugTextString, font);
            DebugText.Color = Color.White;
            DebugText.CharacterSize = 32;

            DebugArea.Draw(DebugText);


            DebugArea.Display();
            DebugAreaSprite.Texture = DebugArea.Texture;
        }


        private void Render()
        {
            _window.Clear(Color.Yellow);
            _window.Draw(PlayAreaSprite);
            _window.Draw(DebugAreaSprite);
            _window.Display();
        }

    }
}
