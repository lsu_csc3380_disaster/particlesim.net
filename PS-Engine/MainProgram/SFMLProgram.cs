﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SFML.Audio;
using SFML.Graphics;
using SFML.System;
using SFML.Window;
using System.Diagnostics;

using PS.ParticleCollection;

namespace PS
{
    public partial class SFMLProgram
    {
        ParticleTree pt;
        Font font;
        
        public void StartSFMLProgram()
        {
            pt = new ParticleTree();
            for(int i = 0; i < 10; i++)
            {
                pt.Add(Particle.generate());
                //Console.Out.Write("\n\n--- " + i + " ---\n\n");
                //pt.Print();
            }
            pt.Print();


            RenderInit();

            while (_window.IsOpen)
            {
                _window.DispatchEvents();
                Time.StepTime();
                GameLoop();
                if (Time.TryStepFixed())
                {
                    RenderPlay();

                    if (Time.TryStepDebug())
                    {
                        RenderDebug();
                    }

                    Render();
                }
            }
        }

        void OnClosed(object sender, EventArgs e)
        {
            _window.Close();
        }

    }
}
